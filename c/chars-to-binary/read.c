#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	char *data;
	int length;
	FILE *fh;

	fh = fopen("data.bin", "r");
	fread(&length, sizeof(int), 1, fh);

	printf("length is: %d\n", length);

	data = malloc(sizeof(char) * length);
	fread(data, sizeof(char), length, fh);

	printf("data: %s", data);

	fclose(fh);
	free(data);

	return 0;
}
