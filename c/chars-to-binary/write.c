#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	int length = 256;
	char *data;
	FILE *fh;

	data = malloc(sizeof(char) * length);

	fgets(data, length, stdin);
	fh = fopen("data.bin", "w+");
	fwrite(&length, sizeof(int), 1, fh);
	fwrite(data, sizeof(char), length, fh);

	free(data);
	fclose(fh);

	return 0;
}
