#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	float vertex;
	char includeLength = 'n';
	int res;
	int length, i;
	FILE *fh;

	printf("Does the vertex binary data file include length header? [Y/n] ");
	fscanf(stdin, "%c", &includeLength);

	fh = fopen("vertices.bin", "r");

	if (includeLength == 'Y') {
		fread(&length, sizeof(int), 1, fh);
		printf("The number of vertices is: %d\n", length);
	}

	printf("Vertices: \n");
	while ((res = fread(&vertex, sizeof(float), 1, fh)) > 0){
		printf("%f\n", vertex );
	};


	printf("\n");
	fclose(fh);

	return 0;
}
