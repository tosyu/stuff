#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	int length, i;
	char includeLength = 'z';
	float *vertex;
	FILE *fh;

	printf("Please supply the number of vertices: ");
	fscanf(stdin, "%d", &length);

	printf("supply %d vertices, one line each:\n", length);
	vertex = malloc(sizeof(float) * length);
	for (i = 0; i < length; i++) {
		fscanf(stdin, "%g", vertex + i);
	}

	fh = fopen("vertices.bin", "w+");

	printf("Should I include the length in the header? [Y/n] ");
	fscanf(stdin, "%c", &includeLength);

	if (includeLength == 'Y') {
		fwrite(&length, sizeof(int), 1, fh);
	}
	
	printf("\nSaving vertices...");
	fwrite(vertex, sizeof(float), length, fh);

	free(vertex);
	fclose(fh);
	printf("\nDone.");

	return 0;
}
