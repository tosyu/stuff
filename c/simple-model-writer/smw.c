#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	int length, i, j, vertexVectorsCount, colorVectorsCount, vertexDataSize, colorDataSize;
	float *vertexData, *colorData;
	FILE *fh;

	printf("Please supply the number of vertices: ");
	fscanf(stdin, "%d", &length);


	vertexVectorsCount = length * 3;
	vertexDataSize = sizeof(float) * vertexVectorsCount;
	colorVectorsCount = vertexVectorsCount + length; /*RGBA*/
	colorDataSize = sizeof(float) * colorVectorsCount;

	vertexData = malloc(vertexDataSize);
	colorData = malloc(colorDataSize);

	printf("supply %d vertices\n", length);
	for (i = 0, j = 1; i < vertexVectorsCount; i++, j++) {
		if (j == 3) {
			printf("Z axis position for vertex %d: ", i / 3 + 1);
			j = 0;
		} else if (j == 2) {
			printf("Y axis position for vertex %d: ", i / 3 + 1);
		} else {
			printf("X axis position for vertex %d: ", i / 3 + 1);
		}
		fscanf(stdin, "%g", vertexData + i);
	}
	
	for (i = 0, j = 1; i < colorVectorsCount; i++, j++) {
		if (j == 4) {
			printf("Alpha value for vertex %d: ", i / 4 + 1);
			j = 0;
		} else if (j == 3) {
			printf("Blue value for vertex %d: ", i / 4 + 1);
		} else if (j == 2) {
			printf("Green value for vertex %d: ", i / 4 + 1);
		} else {
			printf("Red value for vertex %d: ", i / 4 + 1);
		}
		fscanf(stdin, "%g", colorData + i);
	}
	
	fh = fopen("vertices.bin", "w+");

	printf("\nSaving header...\n");
	fwrite(&vertexDataSize, sizeof(int), 1, fh);
	fwrite(&colorDataSize, sizeof(int), 1, fh);
	
	printf("Saving vertices...\n");
	fwrite(vertexData, sizeof(float), vertexVectorsCount, fh);
	fwrite(colorData, sizeof(float), colorVectorsCount, fh);

	free(vertexData);
	free(colorData);
	fclose(fh);
	printf("\nDone.\n");

	return 0;
}
